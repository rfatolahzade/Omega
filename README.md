# Omega Project
Step by step with dotnet web

## Quickstart
Start omega using:
```bash
docker run -it -p 5000:80 --rm ghcr.io/rfinland/omega:master
```
Then 
```bash
curl "http://localhost:5000/POW?a=2&b=3"
```